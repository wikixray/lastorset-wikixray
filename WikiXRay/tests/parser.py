# encoding: utf-8

"""
Unit tests for WikiXRay parsers
"""
try:
    import unittest2 as unittest
except ImportError:
    import unittest

import parsers.dump_sax_stream
import json, sys

class wikiHandlerCallbackTestCase():
    pass

class FixtureMaker(object):
    articles = []

    def make_fixture(self, infile, outfile):
        parser = parsers.dump_sax_stream.wikiHandlerCallback.getParser(lambda article: self.articles.append(article))
        parser.parse(open(infile, 'r'))
        json.dump(self.articles, open(outfile, 'w'))

if __name__ == '__main__':
    if len(sys.argv) == 4 and sys.argv[1] == 'generate':
        FixtureMaker().make_fixture(sys.argv[2], sys.argv[3])
    else:
        print "run in unit-test runner or: parser.py generate <infile> <outfile>"
